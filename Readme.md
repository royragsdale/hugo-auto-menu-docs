# Hugo Auto Menu Theme

This is a theme for [Hugo](https://gohugo.io/), that focuses on auto-generating 
a menu based on the file and folder structure of your content without requiring 
any metadata/frontmatter/configuration.

# How it works

1. Add files and folders under content as you usually would.
2. See them show up as links in navigation menu and pages.
3. Profit.

Currently there is no configuration, and most hugo organizational features like
sections and types aren't implemented.  Basically this theme just publishes your
flat files and nothing more.  I use it as a personal wiki and at the moment have 
only minor updates planned as things bug me.

# Features
- native hugo [lunrjs] search
  - the index is built in a template
  - uses browser localStorage to cache the results
- `wiki` shortcode
  - simplify internal links with `{{<wiki page title>}}` (`title` is optional)

[lunrjs]:https://lunrjs.com/

## Installation
From the top level of you Hugo site run: 

```
$ git clone https://gitlab.com/royragsdale/hugo-auto-menu-docs.git themes/hugo-auto-menu-docs
$ hugo server -t hugo-auto-menu-docs
```

## Credits
The "native" search functionality is adapted from the excellent [docDock][] 
theme.

[docDock]:https://github.com/vjeantet/hugo-theme-docdock
