var lunr, hugoIndex;

function endsWith(str, suffix) {
    return str.indexOf(suffix, str.length - suffix.length) !== -1;
}

function buildIndexFromJson(){
  // Set up lunrjs by declaring the fields we use
  console.log("building...")
  lunr = lunr(function () {
      this.ref('u')     //  uri
      this.field('t')   // title
      this.field('c')   // content

      // Feed lunr with each file and let lunr actually index them
      hugoIndex.forEach(function (doc) {
            this.add(doc)
          }, this)
  })

  localStorage.setItem("lunrData", JSON.stringify(lunr))
  console.log("...done")
}


function getLunrIndex(){
  // check if a fully configured lunr instance is cached
  if( localStorage.getItem("lunrData") != null){
    console.log("best! loaded lunr directly")
    data = JSON.parse(localStorage.getItem("lunrData"));
    lunr = lunr.Index.load(data)
  } else {
    console.log("fetching lunr index")
    $.getJSON(baseurl +"idx.json")
        .done(function(data) {
          localStorage.setItem("lunrData", JSON.stringify(data))
          lunr = lunr.Index.load(data)
        }).fail(function(jqxhr, textStatus, error) {
            var err = textStatus + ", " + error;
            console.error("Error getting Lunr index file:", err);
            console.log("Falling back to manual rebuild");
            buildIndexFromJson()
        });
  }
}

//remove localStorage if site buildtime has changed
function checkStorage(){
  var stored = localStorage.getItem("buildId")
  // either first visit or time has changed need to clear cache
  if (stored === null || stored === undefined || stored != buildId) {
    console.log("clearing  localStorage")
    localStorage.removeItem("hugoIndex");
    localStorage.removeItem("lunrData");
    localStorage.setItem("buildId", buildId)
  }
}

// Initialize search using our generated index file
function initSearch() {
  if (!endsWith(baseurl,"/")){
      baseurl = baseurl+'/'
  };

  checkStorage();

  key = "hugoIndex"
  var item = localStorage.getItem(key)
  if (item != null && item != undefined) {
    hugoIndex = JSON.parse(item);
    console.log("huzzah! got stored:", key)
    getLunrIndex();
  // Otherwise fetch new JSON and store
  } else {
    $.getJSON(baseurl +"index.json")
      .done(function(data) {
        hugoIndex = data;
        localStorage.setItem(key, JSON.stringify(data))
        getLunrIndex();
        })
        .fail(function(jqxhr, textStatus, error) {
            var err = textStatus + ", " + error;
            console.error("Error getting hugoIndex", err);
        });
  }

}

/**
 * Trigger a search in lunr and transform the result
 *
 * @param  {String} query
 * @return {Array}  results
 */
function search(query) {
  // Find the item in our index corresponding to the lunr one to have more info
  var boosted =  query
  if(query.length >= 1){
    boosted = "t:"+query+"^10 c:"+query
  }
  return lunr.search(boosted).map(function(result) {
          return hugoIndex.filter(function(page) {
              return page.u === result.ref;
          })[0];
      });
}

// Let's get started
initSearch();
$( document ).ready(function() {
  var horseyList = horsey($("#search-by").get(0), {
      suggestions: function (value, done) {
          var query = $("#search-by").val();
          var results = search(query);
          done(results);
      },
      filter: function (q, suggestion) {
          return true;
      },
      set: function (value) {
          location.href=value.u;
      },
      render: function (li, suggestion) {
          var uri = suggestion.u.substring(1,suggestion.u.length);

          suggestion.href = baseurl + uri;

          var query = $("#search-by").val();
          var numWords = 2;
          var text = uri.split("/").slice(0,-2).join(" / ")
          suggestion.context = text;
          var image = '<div>' + '» ' + suggestion.t + '</div><div style="font-size:12px">' + (text || '') +'</div>';
          li.innerHTML = image;
      },
      limit: 10
  });
  horseyList.refreshPosition();
});
